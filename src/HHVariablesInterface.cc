#include "Tools/Tools/interface/HHVariablesInterface.h"

HHVariablesInterface::HHVariablesInterface(bool isMC, int KinFit_targetM1, int KinFit_targetM2) {
    isMC_ = isMC;
    KinFit_targetM1_ = KinFit_targetM1;
    KinFit_targetM2_ = KinFit_targetM2;
};

HHVariablesInterface::~HHVariablesInterface() {};

HHvariables HHVariablesInterface::computeHHfeatures(
    std::string category,
    bool hasBoostedAK8, bool hasResolvedAK4, bool hasVBFAK4,
    int pairType, int dau1_index, int dau2_index,
    int bjet1_index, int bjet2_index, int fatjet_index,
    int vbfjet1_index, int vbfjet2_index,
    fRVec muon_pt, fRVec muon_eta, fRVec muon_phi, fRVec muon_mass,
    fRVec ele_pt, fRVec ele_eta, fRVec ele_phi, fRVec ele_mass,
    fRVec tau_pt, fRVec tau_eta, fRVec tau_phi, fRVec tau_mass,
    fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass, fRVec jet_resolution,
    fRVec fatjet_pt, fRVec fatjet_eta, fRVec fatjet_phi, fRVec fatjet_mass,
    float met_pt, float met_phi, float met_covXX, float met_covXY, float met_covYY,
    double Htt_svfit_pt, double Htt_svfit_eta, double Htt_svfit_phi, double Htt_svfit_mass,
    bool bypass_HHKinFit)
{
    HHvariables hhout;

    // full dummy output if no pair found
    if (pairType < 0) { return hhout; }

    std::vector<int> dummyTauVec(tau_pt.size(), -999);
    leps lptns = GetLeps(muon_pt, muon_eta, muon_phi, muon_mass,
                         ele_pt, ele_eta, ele_phi, ele_mass,
                         tau_pt, tau_eta, tau_phi, tau_mass, dummyTauVec,
                         dau1_index, dau2_index, pairType);

    TLorentzVector* dau1_tlv = new TLorentzVector();
    TLorentzVector* dau2_tlv = new TLorentzVector();
    dau1_tlv->SetPtEtaPhiM(lptns.dau1_pt, lptns.dau1_eta, lptns.dau1_phi, lptns.dau1_mass);
    dau2_tlv->SetPtEtaPhiM(lptns.dau2_pt, lptns.dau2_eta, lptns.dau2_phi, lptns.dau2_mass);
    TLorentzVector htt_tlv = *dau1_tlv + *dau2_tlv;
    hhout.Htt_pt   = htt_tlv.Pt();
    hhout.Htt_eta  = htt_tlv.Eta();
    hhout.Htt_phi  = htt_tlv.Phi();
    hhout.Htt_mass = htt_tlv.M();

    TLorentzVector met_tlv = TLorentzVector();
    met_tlv.SetPxPyPzE(met_pt * cos(met_phi), met_pt * sin(met_phi), 0, met_pt);
    TLorentzVector htt_met_tlv = htt_tlv + met_tlv;
    hhout.Htt_met_pt   = htt_met_tlv.Pt();
    hhout.Htt_met_eta  = htt_met_tlv.Eta();
    hhout.Htt_met_phi  = htt_met_tlv.Phi();
    hhout.Htt_met_mass = htt_met_tlv.M();

    if (hasVBFAK4) {
        TLorentzVector vbfjet1_tlv = TLorentzVector();
        TLorentzVector vbfjet2_tlv = TLorentzVector();
        vbfjet1_tlv.SetPtEtaPhiM(jet_pt.at(vbfjet1_index), jet_eta.at(vbfjet1_index),
                                 jet_phi.at(vbfjet1_index), jet_mass.at(vbfjet1_index));
        vbfjet2_tlv.SetPtEtaPhiM(jet_pt.at(vbfjet2_index), jet_eta.at(vbfjet2_index),
                                 jet_phi.at(vbfjet2_index), jet_mass.at(vbfjet2_index));
        TLorentzVector vbfjj_tlv = vbfjet1_tlv + vbfjet2_tlv;
        
        hhout.VBFjj_mass = vbfjj_tlv.M();
        hhout.VBFjj_deltaEta = vbfjet1_tlv.Eta() - vbfjet2_tlv.Eta();
        hhout.VBFjj_deltaPhi = vbfjet1_tlv.Phi() - vbfjet2_tlv.Phi();
    }

    // good htt and vbf + dummy hbb output if no jets found
    if (!hasResolvedAK4 && !hasBoostedAK8) { return hhout; } 

    TLorentzVector hbb_tlv = TLorentzVector();

    if((hasResolvedAK4 && !hasBoostedAK8) ||
        ((hasResolvedAK4 && hasBoostedAK8) &&
         (category == "baseline_bResolved" || category == "control_bResolved" ||
          category == "res1b" || category == "res2b" ||
          category == "vbf_res1b" || category == "vbf_res2b" ||
          category == "base" || category == "baseline" || category == "control" ||
          category == "mutau" || category == "etau" || category == "tautau" ||
          category == "mumu" || category == "ee" || category == "emu" ||
          category == "vbf_2b" || category == "vbf" || 
          category == "vbf_loose" || category == "vbf_tight" ||
          category == "ttbar_invertedMassCR")
        )
      )
    {
        TLorentzVector* bjet1_tlv = new TLorentzVector();
        TLorentzVector* bjet2_tlv = new TLorentzVector();
        bjet1_tlv->SetPtEtaPhiM(jet_pt.at(bjet1_index), jet_eta.at(bjet1_index),
                                jet_phi.at(bjet1_index), jet_mass.at(bjet1_index));
        bjet2_tlv->SetPtEtaPhiM(jet_pt.at(bjet2_index), jet_eta.at(bjet2_index),
                                jet_phi.at(bjet2_index), jet_mass.at(bjet2_index));

        hbb_tlv = *bjet1_tlv + *bjet2_tlv;

        // run HHKinFit
        TVector2* met_tv2 = new TVector2(met_pt * TMath::Cos(met_phi), met_pt * TMath::Sin(met_phi));
        TMatrixD met_cov = TMatrixD(2, 2);
        met_cov[0][0] = met_covXX;
        met_cov[0][1] = met_covXY;
        met_cov[1][0] = met_covXY;
        met_cov[1][1] = met_covYY;

        double bjet1_JER, bjet2_JER;
        if (isMC_) {
            bjet1_JER = bjet1_tlv->E() * jet_resolution.at(bjet1_index);;
            bjet2_JER = bjet2_tlv->E() * jet_resolution.at(bjet2_index);;
        } else {
            bjet1_JER = -1;
            bjet2_JER = -1;
        }

        if (!bypass_HHKinFit)
        {
            HHKinFit2::HHKinFitMasterHeavyHiggs m_HHKinFit(
                *bjet1_tlv, *bjet2_tlv, *dau1_tlv, *dau2_tlv,
                *met_tv2, met_cov, bjet1_JER, bjet2_JER, false, TLorentzVector(0,0,0,0));
            m_HHKinFit.addHypo(KinFit_targetM1_, KinFit_targetM2_);

            bool good_fit = true;
            try {
                m_HHKinFit.fit();
            }
            catch (HHKinFit2::HHInvMConstraintException &e) {
                good_fit = false;
            }
            catch (HHKinFit2::HHEnergyRangeException &e) {
                good_fit = false;
            }
            catch (HHKinFit2::HHEnergyConstraintException &e) {
                good_fit = false;
            }

            if (good_fit) {
                hhout.HH_kinfit_mass = m_HHKinFit.getMH();
                hhout.HH_kinfit_chi2 = m_HHKinFit.getChi2();
            };
        }
    }
    else if ((!hasResolvedAK4 && hasBoostedAK8) ||
             ((hasResolvedAK4 && hasBoostedAK8) &&
              (category == "baseline_bBoosted" || category == "control_bBoosted" ||
               category == "boosted" || category == "vbf_boosted")
             )
            )
    {
        hbb_tlv.SetPtEtaPhiM(fatjet_pt.at(fatjet_index), fatjet_eta.at(fatjet_index),
                             fatjet_phi.at(fatjet_index), fatjet_mass.at(fatjet_index));

        // FIXME: should m_HHKinFit be set to be identical to Htt+Hbb ???
    }

    hhout.Hbb_pt   = hbb_tlv.Pt();
    hhout.Hbb_eta  = hbb_tlv.Eta();
    hhout.Hbb_phi  = hbb_tlv.Phi();
    hhout.Hbb_mass = hbb_tlv.M();

    TLorentzVector hh_tlv = htt_tlv + hbb_tlv;
    hhout.HH_pt   = hh_tlv.Pt();
    hhout.HH_eta  = hh_tlv.Eta();
    hhout.HH_phi  = hh_tlv.Phi();
    hhout.HH_mass = hh_tlv.M();

    if (Htt_svfit_pt > 0) {
        TLorentzVector htt_svfit_tlv = TLorentzVector();
        htt_svfit_tlv.SetPtEtaPhiM(Htt_svfit_pt, Htt_svfit_eta,
                                   Htt_svfit_phi, Htt_svfit_mass);
        TLorentzVector hh_svfit_tlv = htt_svfit_tlv + hbb_tlv;
        hhout.HH_svfit_pt = hh_svfit_tlv.Pt();
        hhout.HH_svfit_eta = hh_svfit_tlv.Eta();
        hhout.HH_svfit_phi = hh_svfit_tlv.Phi();
        hhout.HH_svfit_mass = hh_svfit_tlv.M();
    }

    // return full set output
    return hhout;

}