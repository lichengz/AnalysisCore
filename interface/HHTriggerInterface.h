#ifndef HHTriggerInterface_h
#define HHTriggerInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class HHTriggerInterface                                                                                      //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <string>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

// Utils
#include "Tools/Tools/interface/HHUtils.h"

// HHTriggerInterface class
class HHTriggerInterface {

  public:
    HHTriggerInterface ();

    ~HHTriggerInterface ();

    trigger_output get_trigger_output(
      fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_phi, fRVec Muon_mass,
      fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_phi, fRVec Electron_mass,
      fRVec Tau_pt, fRVec Tau_eta, fRVec Tau_phi, fRVec Tau_mass,
      fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
      int pairType, int dau1_index, int dau2_index,
      bool hasResolvedAK4, bool hasBoostedAK8, bool hasVBFAK4,
      int bjet1_index, int bjet2_index, int vbfjet1_index, int vbfjet2_index,
      iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
      std::vector<trig_req> hlt_mu_triggers, std::vector<trig_req> hlt_ele_triggers,
      std::vector<trig_req> hlt_mutau_triggers, std::vector<trig_req> hlt_eletau_triggers,
      std::vector<trig_req> hlt_ditau_triggers, std::vector<trig_req> hlt_ditaujet_triggers,
      std::vector<trig_req> hlt_quadjet_triggers, std::vector<trig_req> hlt_vbf_triggers,
      std::string dataset, bool isMC);
};

#endif
