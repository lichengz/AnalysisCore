#ifndef HHTrigSFinterface_corrlib_h
#define HHTrigSFinterface_corrlib_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class HHTrigSFinterface_corrlib                                                                              //
//                                                                                                                //
//   Class to compute HH trigger scale factors.                                                                   //
//                                                                                                                //
//   Author: Jona Motta                                                                                           //
//   Date  : Apr 2024                                                                                             //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>


// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <Math/VectorUtil.h>
#include <TH3.h>
#include <TFile.h>

// CMSSW
#include "HTT-utilities/LepEffInterface/interface/ScaleFactor.h"
#include "TauAnalysisTools/TauTriggerSFs/interface/SFProvider.h"

// Correctionlib
#include "Base/Modules/interface/correctionWrapper.h"

// Utils
#include "Tools/Tools/interface/HHUtils.h"

typedef ROOT::VecOps::RVec<float> fRVec;
typedef ROOT::VecOps::RVec<bool> bRVec;
typedef ROOT::VecOps::RVec<int> iRVec;

// HHTrigSFinterface_corrlib class
class HHTrigSFinterface_corrlib {
  
  public:
    HHTrigSFinterface_corrlib(float year,
      float isomuTrg_offMuThr, float mutauTrg_offMuThr, float mutauTrg_offTauThr,
      float eleTrg_offEleThr, float etauTrg_offEleThr, float etauTrg_offTauThr,
      float ditauTrg_offTauThr, float ditaujetTrg_offTauThr, float ditaujetTrg_offJetThr,
      std::string eTrgSF_file, std::string eCorrHltName, std::string eCorrYearTag,
      std::string muTrgSF_file, std::string muCorrHltName,
      std::string tauTrgSF_file,
      std::string eTauTrgSF_file, std::string etauCorrHltName,
      std::string muTauTrgSF_file, std::string muTauCorrHltName,
      std::string taujetTrgSF_file, std::string taujetCorrHltName,
      std::string vbfTrgSF_file, std::string tauCorr_vbfTrg, std::string jetCorr_vbfTrg);

    triggerSF_output get_scale_factors(int pairType, bool isVBFtrigger,
      std::string TauvsJetWP, float bjet1_pt, float bjet1_eta,
      int dau1_decayMode, float dau1_pt, float dau1_eta,
      int dau2_decayMode, float dau2_pt, float dau2_eta,
      float vbfjet1_pt, float vbfjet1_eta, float vbfjet1_phi, float vbfjet1_mass,
      float vbfjet2_pt, float vbfjet2_eta, float vbfjet2_phi, float vbfjet2_mass);

    triggerSF_output get_hh_trigsf(int pairType, bool isVBFtrigger,
      int bjet1_idx, int fatjet_idx, int dau1_index, int dau2_index,
      int vbfjet1_index, int vbfjet2_index,
      fRVec muon_pt, fRVec muon_eta, fRVec electron_pt, fRVec electron_eta,
      fRVec tau_pt, fRVec tau_eta, iRVec tau_decayMode, std::string tau_vsJetWP,
      fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass,
      fRVec fatjet_pt, fRVec fatjet_eta);

    ~HHTrigSFinterface_corrlib ();

  private:
    std::vector <int> decayModes = {0, 1, 10, 11};
    
    MyCorrections eCorr_eTrg_sf_        = MyCorrections();
    MyCorrections eCorr_eTrg_effMC_     = MyCorrections();
    MyCorrections eCorr_eTrg_effData_   = MyCorrections();
    MyCorrections muCorr_muTrg_sf_      = MyCorrections();
    MyCorrections muCorr_muTrg_effMC_   = MyCorrections();
    MyCorrections muCorr_muTrg_effData_ = MyCorrections();
    MyCorrections tauCorr_              = MyCorrections();

    MyCorrections eCorr_etauTrg_effMC_     = MyCorrections();
    MyCorrections eCorr_etauTrg_effData_   = MyCorrections();
    MyCorrections muCorr_mutauTrg_effMC_   = MyCorrections();
    MyCorrections muCorr_mutauTrg_effData_ = MyCorrections();
    
    MyCorrections jetCorr_taujetTrg_ = MyCorrections();

    MyCorrections tauCorr_vbfTrg_  = MyCorrections();
    MyCorrections jetCorr_vbfTrg_  = MyCorrections();
    
    std::string eCorrYearTag_;
    std::string eCorrHltName_;
    std::string etauCorrHltName_;

    float isomuTrg_offMuThr_;  // equivalent of mutau_pt_th1_ for Run2 interface !!
    float mutauTrg_offMuThr_;
    float mutauTrg_offTauThr_; // equivalent of mutau_pt_th2_ for Run2 interface !!
    float eleTrg_offEleThr_;   // equivalent of etau_pt_th1_ for Run2 interface !!
    float etauTrg_offEleThr_;
    float etauTrg_offTauThr_;  // equivalent of etau_pt_th2_ for Run2 interface !!
    float ditauTrg_offTauThr_;
    float ditaujetTrg_offTauThr_;
    float ditaujetTrg_offJetThr_;
};

#endif // HHTrigSFinterface_corrlib
