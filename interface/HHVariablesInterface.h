#ifndef HHVariablesInterface_h
#define HHVariablesInterface_h

// -------------------------------------------------------------------------- //
//                                                                            //
//   class HHVariablesInterface                                               //
//                                                                            //
// -------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <Math/VectorUtil.h>
#include <TMatrixD.h>
#include <TVector2.h>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

// Utils
#include "Tools/Tools/interface/HHUtils.h"

// HHKinFit2 libraries
#include "HHKinFit2/HHKinFit2Scenarios/interface/HHKinFitMasterHeavyHiggs.h"
#include "HHKinFit2/HHKinFit2Core/interface/exceptions/HHInvMConstraintException.h"
#include "HHKinFit2/HHKinFit2Core/interface/exceptions/HHEnergyRangeException.h"
#include "HHKinFit2/HHKinFit2Core/interface/exceptions/HHEnergyConstraintException.h"

class HHVariablesInterface {

    public:
        HHVariablesInterface (bool isMC, int KinFit_targetM1, int KinFit_targetM2);
        ~HHVariablesInterface ();

        HHvariables computeHHfeatures(std::string category,
            bool hasBoostedAK8, bool hasResolvedAK4, bool hasVBFAK4,
            int pairType, int dau1_index, int dau2_index,
            int bjet1_index, int bjet2_index, int fatjet_index,
            int vbfjet1_index, int vbfjet2_index,
            fRVec muon_pt, fRVec muon_eta, fRVec muon_phi, fRVec muon_mass,
            fRVec ele_pt, fRVec ele_eta, fRVec ele_phi, fRVec ele_mass,
            fRVec tau_pt, fRVec tau_eta, fRVec tau_phi, fRVec tau_mass,
            fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass, fRVec jet_resolution,
            fRVec fatjet_pt, fRVec fatjet_eta, fRVec fatjet_phi, fRVec fatjet_mass,
            float met_pt, float met_phi, float met_covXX, float met_covXY, float met_covYY,
            double Htt_svfit_pt, double Htt_svfit_eta, double Htt_svfit_phi, double Htt_svfit_mass,
            bool bypass_HHKinFit
        );

    private:
        bool isMC_;
        int KinFit_targetM1_;
        int KinFit_targetM2_;
};

#endif // HHVariablesInterface_h