import os
from array import array

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from Base.Modules.baseModules import JetLepMetSyst
from analysis_tools.utils import import_root

ROOT = import_root()

class HHVarRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHVarRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.noJER = kwargs.pop("noJER", False)
        self.isZZAnalysis = kwargs.pop("isZZAnalysis", False)
        self.bypass_HHKinFit = kwargs.pop("bypass_HHKinFit", False)
        self.ctgrName = kwargs.pop("ctgrName")

        localIsMC = ("true" if self.isMC else "false")
        self.jet_resolution = "jet_pt_resolution" if self.isMC else "Jet_eta"
        if self.noJER:
            localIsMC = "false"
            self.jet_resolution = "Jet_eta" # dummy/placholder

        if not self.isZZAnalysis:
            KinFit_targetM1 = 125
            KinFit_targetM2 = 125
        else:
            KinFit_targetM1 = 91
            KinFit_targetM2 = 91

        if not os.getenv("_HHVAR_%s" % self.systs):
            os.environ["_HHVAR_%s" % self.systs] = "hhvar"

            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")
            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/HHVariablesInterface.h".format(base))

            ROOT.gInterpreter.Declare("""
                auto HHcomputer = HHVariablesInterface(%s, %i, %i);
            """ % (localIsMC, KinFit_targetM1, KinFit_targetM2))

    def run(self, df):
        variables_str = ("Htt_pt,Htt_eta,Htt_phi,Htt_mass,"
                         "Htt_met_pt,Htt_met_eta,Htt_met_phi,Htt_met_mass,"
                         "Hbb_pt,Hbb_eta,Hbb_phi,Hbb_mass,"
                         "HH_pt,HH_eta,HH_phi,HH_mass,"
                         "HH_svfit_pt,HH_svfit_eta,HH_svfit_phi,HH_svfit_mass,"
                         "HH_kinfit_mass,HH_kinfit_chi2,"
                         "VBFjj_deltaEta,VBFjj_deltaPhi,VBFjj_mass")
        variables = variables_str.split(",")

        df = df.Define("hhfeatures%s" % self.systs,
                       "HHcomputer.computeHHfeatures(\"{9}\", "
                       "hasBoostedAK8, hasResolvedAK4, hasVBFAK4, "
                       "pairType, dau1_index, dau2_index, "
                       "bjet1_JetIdx, bjet2_JetIdx, fatjet_JetIdx, "
                       "VBFjet1_JetIdx, VBFjet2_JetIdx, "
                       "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
                       "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
                       "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, "
                       "Jet_pt{3}, Jet_eta, Jet_phi, Jet_mass{3}, {7}, "
                       "FatJet_pt{3}, FatJet_eta, FatJet_phi, FatJet_mass{3}, "
                       "PuppiMET{5}_pt{4}, PuppiMET{5}_phi{4}, "
                       "MET_covXX, MET_covXY, MET_covYY, "
                       "Htt_svfit_pt{6}, Htt_svfit_eta{6}, Htt_svfit_phi{6}, "
                       "Htt_svfit_mass{6}, {8})".format(
                            self.muon_syst, self.electron_syst, self.tau_syst, self.jet_syst,
                            self.met_syst, self.met_smear_tag, self.systs, self.jet_resolution,
                            "true" if self.bypass_HHKinFit else "false", self.ctgrName))

        features = []
        for var in variables:
            df = df.Define(f"{var}{self.systs}", f"hhfeatures{self.systs}.{var}")
            features.append(f"{var}{self.systs}")

        return df, features

def HHVarRDF(*args, **kwargs):
    return lambda: HHVarRDFProducer(*args, **kwargs)
