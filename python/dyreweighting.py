import os
from array import array
import math

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from Base.Modules.baseModules import DummyModule

from analysis_tools.utils import import_root
ROOT = import_root()


# ---- DYstitching
class DYstitchingProducer(Module):
    def __init__(self, year, *args, **kwargs):
        isUL = "true" if kwargs.pop("isUL") else "false"
        super(DYstitchingProducer, self).__init__(*args, **kwargs)
        if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gSystem.Load("libToolsTools.so")

        base = "{}/{}/src/Tools/Tools".format(
            os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
        ROOT.gROOT.ProcessLine(".L {}/interface/DYstitching.h".format(base))

        self.dy_reweighter = ROOT.DYstitching(year, isUL)

    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree

        self.out.branch('DYstitchWeight', 'F')
        pass

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    def analyze(self, event):
        """process event, return True (go to next module) or False (fail, go to next event)"""
        weight = self.dy_reweighter.get_stitching_weight(
            event.LHE_Nb, event.LHE_Njets, event.LHE_HT)
        self.out.fillBranch("DYstitchWeight", weight)
        return True


class DYstitchingRDFProducer():
    """ Read Drell-Yan stitching weights from correctionlib """

    def __init__(self, year, isDY, *args, **kwargs):
        self.isDY = isDY
        self.runPeriod = kwargs.pop("runPeriod")

        json_input = os.environ['CMSSW_BASE'] + f"/src/Tools/Tools/data/DYstitching/{year}{self.runPeriod}/dy_stitch_weights.json"

        if self.isDY:
            os.environ["_DYstitchingRDFProducer"] = "_DYstitchingRDFProducer"

            # Load libraries
            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")

            if not ROOT.gInterpreter.IsLoaded("$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"):
                ROOT.gInterpreter.ProcessLine(os.path.expandvars(
                    '.L $CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h'))

            # Declare DYstitching correctionlib instance
            ROOT.gInterpreter.ProcessLine(
                'auto corr_DYstitching = MyCorrections("%s", "drellYanStitchWeight");' % json_input
            )

    def run(self, df):
        if self.isDY:
            df = df.Define("DYstitchWeight", "corr_DYstitching.eval({LHE_NpNLO, LHE_Vpt}) ")
        else:
            df = df.Define("DYstitchWeight", "1")
        return df, ["DYstitchWeight"]


def DYstitchingRDF(*args, **kwargs):
    isDY = kwargs.pop("isDY", False)
    year = kwargs.pop("year")

    # Run 2
    if year <= 2018:
        if isDY:
            return lambda: DYstitchingProducer(year=year, **kwargs)
        else:
            return lambda: DummyModule(**kwargs)

    # Run 3
    else:
        return lambda: DYstitchingRDFProducer(year=year, isDY=isDY, *args, **kwargs)


# ---- DYscaling not actually used in CCLUB Run 3
class DYscalingRDFProducer():
    def __init__(self, year, isDY, *args, **kwargs):
        self.isDY = isDY
        if self.isDY:
            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")

            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/DYscaling.h".format(base))
            ROOT.gInterpreter.Declare('auto dy_scaler = DYscaling(%s);' % year)

    def run(self, df):
        branches = ["DYscale_LL", "DYscale_MM", "DYscale_MH", "DYscale_MTT",
            "DYscale_MTT_up", "DYscale_MTT_down"]
        if self.isDY:
            df = df.Define("dyscalingweights",
                "dy_scaler.get_dy_scale(GenJet_pt, GenJet_eta, GenJet_phi, GenJet_mass,"
                    "GenJet_hadronFlavour, LHE_Nb, GenPart_pt, GenPart_eta, GenPart_phi,"
                    " GenPart_mass, GenPart_statusFlags, GenPart_pdgId)")

            for ib, branch in enumerate(branches):
                df = df.Define(branch, "dyscalingweights[%s]" % ib)
        else:
            for branch in branches:
                df = df.Define(branch, "1")
        return df, branches


def DYscalingRDF(*args, **kwargs):
    isDY = kwargs.pop("isDY", False)
    year = kwargs.pop("year")

    return lambda: DYscalingRDFProducer(year=year, isDY=isDY, *args, **kwargs)
