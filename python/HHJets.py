import os
from array import array

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from analysis_tools.utils import import_root
from Tools.Tools.jet_utils import JetPair
from Tools.Tools.utils_trigger import get_trigger_requests
from Base.Modules.baseModules import JetLepMetSyst

ROOT = import_root()


class HHJetsRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHJetsRDFProducer, self).__init__(self, *args, **kwargs)
        self.extEtaAcc = kwargs.pop("extEtaAcc")
        self.btag_algo = kwargs.pop("btag_algo")
        self.btag_algo_cut = kwargs.pop("btag_algo_cut", "medium")
        self.btag_algo_wps = kwargs.pop("btag_algo_wps")
        self.bypass_HHBTag = kwargs.pop("bypass_HHBTag", False)
        self.bpair_filter = kwargs.pop("bpair_filter")
        self.subjet_match = kwargs.pop("subjet_match", False)

        if not os.getenv("_HHJets"):
            os.environ["_HHJets"] = "HHJets"

            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")
            if os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc10":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc10/"
                    "external/eigen/d812f411c3f9-cms/include/")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc10/external/"
                    "tensorflow/2.5.0/include/")
            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc820":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc820/"
                    "external/eigen/d812f411c3f9-bcolbf/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc820/"
                    "external/tensorflow/2.1.0-bcolbf/include")

            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "el9_amd64_gcc12":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/el9_amd64_gcc12/"
                    "external/eigen/3bb6a48d8c171cf20b5f8e48bfb4e424fbd4f79e-ff59ad69bf3dc401976591897b386b6a/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/el9_amd64_gcc12/"
                    "external/tensorflow/2.12.0-56ca17f4dd95f093a501c2ddea2603f2/include")
            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc12":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc12/"
                    "external/eigen/3bb6a48d8c171cf20b5f8e48bfb4e424fbd4f79e-ff59ad69bf3dc401976591897b386b6a/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc12/"
                    "external/tensorflow/2.12.0-a78fc3db7a6d73e4417418af1388a3c7/include")
            else:
                raise ValueError("Architecture not considered")

            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))

            ROOT.gROOT.ProcessLine(".L {}/interface/HHJetsInterface.h".format(base))

            self.year = kwargs.pop("year")
            base_hhbtag = "{}/{}/src/HHTools/HHbtag".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            
            if self.year == 2016 or self.year == 2017 or self.year == 2018:
                models = [base_hhbtag + "/models/HHbtag_v2_par_%i" % i for i in range(2)]
            
            elif self.year == 2022 or self.year == 2023:
                models = [base_hhbtag + "/models/HHbtag_v3_par_%i" % i for i in range(2)]

            ROOT.gInterpreter.Declare("""
                auto HHJets = HHJetsInterface("%s", "%s", %s, "%s", %i, %s, %f);
            """ % (models[0], models[1], int(self.year), self.runPeriod, self.extEtaAcc,
                   "true" if self.bypass_HHBTag else "false",
                   self.btag_algo_wps[self.btag_algo_cut]))
            
    def run(self, df):
        # use the old subjet matching approach
        if self.subjet_match:
            raise ValueError("The old approach matching two AK4 subjets to the "
                             "boosted AK8 jet has been decomissioned.")

        # use the new fatjet without subjet apporoach
        else:
            # use the chs jets with puID
            if self.year <= 2018:
                df = df.Define("HHJets", "HHJets.GetHHJetsWrapper(event, true, "
                    "Jet_pt{5}, Jet_eta, Jet_phi, Jet_mass{5}, "
                    "Jet_puId, Jet_jetId, Jet_btag{6}, "
                    "FatJet_pt{5}, FatJet_eta, FatJet_phi, FatJet_mass{5}, "
                    "FatJet_msoftdrop, FatJet_particleNetWithMass_HbbvsQCD, "
                    "pairType, dau1_index, dau2_index, "
                    "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
                    "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
                    "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, "
                    "MET{4}_pt{3}, MET{4}_phi{3})".format(
                        self.muon_syst, self.electron_syst, self.tau_syst, self.met_syst,
                        self.met_smear_tag, self.jet_syst, self.btag_algo))
            # use the puppi jets without puID
            else:
                df = df.Define("HHJets", "HHJets.GetHHJetsWrapper(event, false, "
                    "Jet_pt{5}, Jet_eta, Jet_phi, Jet_mass{5}, "
                    "Jet_jetId, " # dummy not used for PUPPI
                    "Jet_jetId, Jet_btag{6}, "
                    "FatJet_pt{5}, FatJet_eta, FatJet_phi, FatJet_mass{5}, "
                    "FatJet_msoftdrop, FatJet_particleNetWithMass_HbbvsQCD, "
                    "pairType, dau1_index, dau2_index, "
                    "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
                    "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
                    "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, "
                    "PuppiMET{4}_pt{3}, PuppiMET{4}_phi{3})".format(
                        self.muon_syst, self.electron_syst, self.tau_syst, self.met_syst,
                        self.met_smear_tag, self.jet_syst, self.btag_algo))

        df = df.Define("Jet_HHbtag", "HHJets.hhbtag")

        df = df.Define("hasResolvedAK4", "HHJets.hasResolvedAK4")
        df = df.Define("bjet1_JetIdx", "HHJets.bjet_idx1")
        df = df.Define("bjet2_JetIdx", "HHJets.bjet_idx2")

        df = df.Define("hasVBFAK4", "HHJets.hasVBFAK4")
        df = df.Define("VBFjet1_JetIdx", "HHJets.vbfjet_idx1")
        df = df.Define("VBFjet2_JetIdx", "HHJets.vbfjet_idx2")

        df = df.Define("ctjet_indexes", "HHJets.ctjet_indexes")
        df = df.Define("fwjet_indexes", "HHJets.fwjet_indexes")

        df = df.Define("hasBoostedAK8", "HHJets.hasBoostedAK8")
        df = df.Define("fatjet_JetIdx", "HHJets.fatjet_idx")

        df = df.Define("fatjet_Jet1_JetIdx", "HHJets.fatjet2jet1_idx")
        df = df.Define("fatjet_Jet2_JetIdx", "HHJets.fatjet2jet1_idx")

        if self.bpair_filter:
            df = df.Filter("bjet1_JetIdx >= 0 || hasBoostedAK8")

        return df, ["Jet_HHbtag", "hasResolvedAK4", "bjet1_JetIdx", "bjet2_JetIdx",
                    "hasVBFAK4", "VBFjet1_JetIdx", "VBFjet2_JetIdx", "ctjet_indexes",
                    "fwjet_indexes", "hasBoostedAK8", "fatjet_JetIdx", "fatjet_Jet1_JetIdx", "fatjet_Jet2_JetIdx"]


def HHJetsRDF(**kwargs):
    """
    Returns the HHbtag output, the indexes from the 2 bjets and 2 vbfjets (if existing),
    the indexes of the additional central and forward jets (if existing) and if the
    event has a boosted topology.

    Lepton and jet systematics (used for pt and mass variables) can be modified using the parameters
    from :ref:`BaseModules_JetLepMetSyst`.

    :param extEtaAcc: extend bjet eta cut to 2.5 in Run-3 instead of 2.4 for Run-2
    :type extEtaAcc: bool

    :param btag_algo: btag algorithm to apply
    :type btag_algo: string

    :param btag_algo_wps: working points of the btag algorithm
    :type btag_algo_wps: string

    :param btag_algo_cut: working point to be applied for the btag algorithm
    :type btag_algo_cut: string

    :param bypass_HHBTag: apply HHBTag algorithm or bypass it
    :type bypass_HHBTag: bool

    :param bpair_filter: whether to filter out output events if they don't have 2 bjet candidates
    :type bpair_filter: bool

    :param subjet_match: whether to match AK4 subjets to AK8 jet
    :type subjet_match: bool

    :param tau_syst: suffix to variable branch name
    :type tau_syst: string

    :param jet_syst: suffix to variable branch name
    :type jet_syst: string

    :param met_smear_tag: suffix to variable branch name
    :type met_smear_tag: string


    YAML sintax:

    .. code-block:: yaml

        codename:
            name: HHJetsRDF
            path: Tools.Tools.HHJets
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                extEtaAcc: True/False
                btag_algo: self.config.btag_algo
                btag_algo_wps: self.config.btag_algo_wps
                btag_algo_cut: "M"
                bypass_HHBTag: False/True
                bpair_filter: True/False
                subjet_match: False/True
                tau_syst: "corr_Medium"
                jet_syst: "corr"
                met_smear_tag: ""

    """
    return lambda: HHJetsRDFProducer(**kwargs)

class HHJetsVarRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHJetsVarRDFProducer, self).__init__(*args, **kwargs)
        self.btag_algo = kwargs.pop("btag_algo")

        if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gSystem.Load("libToolsTools.so")

        base = "{}/{}/src/Tools/Tools".format(
            os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
        
        if not ROOT.gInterpreter.IsLoaded("{}/interface/HHUtils.h".format(base)):
            ROOT.gROOT.ProcessLine(".L {}/interface/HHUtils.h".format(base))

    def run(self, df):
        variables_str = ("bjet1_pt{0},bjet1_eta,bjet1_phi,bjet1_mass{0},bjet1_btag,"
                         "bjet2_pt{0},bjet2_eta,bjet2_phi,bjet2_mass{0},bjet2_btag,"
                         "fatbjet_pt{0},fatbjet_eta,fatbjet_phi,fatbjet_mass{0},fatbjet_btag,"
                         "vbfjet1_pt{0},vbfjet1_eta,vbfjet1_phi,vbfjet1_mass{0},"
                         "vbfjet2_pt{0},vbfjet2_eta,vbfjet2_phi,vbfjet2_mass{0}"
                         "".format(self.jet_syst))
        variables = variables_str.split(",")

        all_branches = df.GetColumnNames()
        if variables[0] in all_branches:
            return df, []

        df = df.Define("jetsfeatures%s" % self.jet_syst, "GetJets("
                "hasResolvedAK4, bjet1_JetIdx, bjet2_JetIdx, "
                "hasVBFAK4, VBFjet1_JetIdx, VBFjet2_JetIdx, "
                "hasBoostedAK8, fatjet_JetIdx, "
                "Jet_pt{0}, Jet_eta, Jet_phi, Jet_mass{0}, Jet_btag{1}, "
                "FatJet_pt{0}, FatJet_eta, FatJet_phi, FatJet_mass{0}, "
                "FatJet_particleNetWithMass_HbbvsQCD)".format(self.jet_syst, self.btag_algo))

        features = []
        for var in variables:
            df = df.Define(var, f"jetsfeatures{self.jet_syst}.{var.replace(self.jet_syst,'')}")
            features.append(var)

        return df, features


def HHJetsVarRDF(**kwargs):
    """
    Returns the pt and mass of the selected bjets possibly including systematics

    Jets systematics can be modified using the parameters from
    :ref:`BaseModules_JetLepMetSyst`.

    Required RDFModules: :ref:`HHJetsRDF`.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHJetsVarRDF
            path: Tools.Tools.HHJets
            parameters:
                isMC: self.dataset.process.isMC

    """
    return lambda: HHJetsVarRDFProducer(**kwargs)
