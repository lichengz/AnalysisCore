import os

from analysis_tools.utils import import_root

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from Tools.Tools.utils_trigger import get_trigger_requests
from Base.Modules.baseModules import JetLepMetSyst

ROOT = import_root()

class HHLeptonRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHLeptonRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.pairType_filter = kwargs.pop("pairType_filter", "signals")
        self.tauId_algo = kwargs.pop("tauId_algo")
        self.tauId_algo_wps = kwargs.pop("tauId_algo_wps")
        self.applyOflnIso = kwargs.pop("applyOflnIso", True)

        if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gSystem.Load("libToolsTools.so")

        base = "{}/{}/src/Tools/Tools".format(
            os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
        ROOT.gROOT.ProcessLine(".L {}/interface/HHLeptonInterface.h".format(base))
        
        ROOT.gInterpreter.Declare("""
            auto HHLepton = HHLeptonInterface(%s, %s, %s, %s, %s, %s);
        """ % (self.tauId_algo_wps["vsjet"]["VVVLoose"],
               self.tauId_algo_wps["vse"]["VLoose"],
               self.tauId_algo_wps["vse"]["VVLoose"],
               self.tauId_algo_wps["vsmu"]["Tight"],
               self.tauId_algo_wps["vsmu"]["VLoose"],
               "true" if self.applyOflnIso else "false"))

    def run(self, df):
        variables = ["pairType", "dau1_index", "dau2_index", "isOS",
            "dau1_eta", "dau1_phi", "dau1_iso", "dau1_eleMVAiso", "dau1_decayMode",
            "dau1_tauIdVSe", "dau1_tauIdVSmu",
            "dau1_tauIdVSjet",
            "dau2_eta", "dau2_phi", "dau2_iso", "dau2_decayMode",
            "dau2_tauIdVSe", "dau2_tauIdVSmu",
            "dau2_tauIdVSjet"
        ]

        all_branches = df.GetColumnNames()
        Electron_mvaIso_WP80 = "Electron_mvaIso_WP80"
        if Electron_mvaIso_WP80 not in all_branches:
            Electron_mvaIso_WP80 = "Electron_mvaFall17V2Iso_WP80"
        Electron_mvaIso_WP90 = "Electron_mvaIso_WP90"
        if Electron_mvaIso_WP90 not in all_branches:
            Electron_mvaIso_WP90 = "Electron_mvaFall17V2Iso_WP90"
        Electron_mvaNoIso_WP90 = "Electron_mvaNoIso_WP90"
        if Electron_mvaNoIso_WP90 not in all_branches:
            Electron_mvaNoIso_WP90 = "Electron_mvaFall17V2noIso_WP90"

        df = df.Define("hh_lepton_pairs_results", "HHLepton.get_dau_indexes("
            "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
            "Muon_pfRelIso04_all, Muon_dxy, Muon_dz, Muon_mediumId, Muon_tightId, Muon_charge, "
            "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
            "{3}, {4}, {5}, Electron_pfRelIso03_all,  "
            "Electron_dxy, Electron_dz, Electron_charge, Electron_mvaIso, "
            "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, "
            "Tau_{6}VSmu, Tau_{6}VSe, "
            "Tau_{6}VSjet, Tau_raw{7}VSjet, "
            "Tau_dz, Tau_decayMode, Tau_charge"
            ")".format(self.muon_syst, self.electron_syst, self.tau_syst,
            Electron_mvaIso_WP80, Electron_mvaNoIso_WP90, Electron_mvaIso_WP90,
            self.tauId_algo, self.tauId_algo[2:]))

        for var in variables:
            df = df.Define(var, "hh_lepton_pairs_results.%s" % var)

        # Parse pairType_filter looking for additional channels...
        filters = self.pairType_filter.split(",")
        for filt in filters:
            if filt not in ["all", "signals", "0", "1", "2", "3", "4", "5"]:
                raise ValueError("pairType_filter with value {} is not valid".format(filt))
            
        add_chnls = [ filt for filt in filters if filt == "0" or filt == "1" or
                                                  filt == "2" or filt == "3" or
                                                  filt == "4" or filt == "5" ]

        # ...and compose final selection string
        selection_string = ""
        if "signals" in filters:
            selection_string = "(pairType == 0 || pairType == 1 || pairType == 2)"
        elif "all" in filters:
            selection_string = "(pairType >= -1)"

        for add_chnl in add_chnls:
            if selection_string == "":
                selection_string += "(pairType == {})".format(add_chnl)
            else:
                selection_string += " || (pairType == {})".format(add_chnl)
        # Check final selectin string...
        if selection_string == "":
            raise ValueError("No pairType_filter requested! Please set at least one option among: "
                             "all, signals, or specific channels numbers [0, 1, 2, 3, 4, 5]")

        # ...and filter the RDF with with
        df = df.Filter(selection_string)

        return df, variables

def HHLeptonRDF(**kwargs):
    """
    Returns the index of the two selected taus + several of their variables not affected by systematics.

    Lepton systematics (used for pt and mass variables) can be modified using the parameters from 
    :ref:`BaseModules_JetLepMetSyst`.

    :param NanoAODv: version of the NanoAOD production
    :type NanoAODv: str

    :param tauId_algo: Tau ID algorthm to be used
    :type tauId_algo: string

    :param tauId_algo_wps: Tau ID algorthm to be used
    :type tauId_algo_wps: dict

    :param pairType_filter: whether to filter out output events if they don't have 2 lepton candidates
    :type pairType_filter: bool

    :param applyOflnIso: whether to apply the offline Id&Iso selections
    :type applyOflnIso: bool

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHLeptonRDF
            path: Tools.Tools.HHLepton
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                tauId_algo: self.config.tauId_algo
                tauId_algo_wps: self.config.tauId_algo_wps
                pairType_filter: True
                applyOflnIso: True

    """
    return lambda: HHLeptonRDFProducer(**kwargs)



class HHLeptonVarRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHLeptonVarRDFProducer, self).__init__(*args, **kwargs)

        if not os.getenv("DAU_VAR"):
            os.environ["DAU_VAR"] = "true"
            ROOT.gInterpreter.Declare("""
                using Vfloat = const ROOT::RVec<float>&;
                using VInt = const ROOT::RVec<int>&;
                std::vector<float> get_lepton_values (
                    int pairType, int dau1_index, int dau2_index,
                    Vfloat muon_pt, Vfloat muon_mass,
                    Vfloat electron_pt, Vfloat electron_mass,
                    Vfloat tau_pt, Vfloat tau_mass
                )
                {
                    float dau1_pt, dau1_mass, dau2_pt, dau2_mass;
                    if (pairType == 0) {
                        dau1_pt = muon_pt.at(dau1_index);
                        dau1_mass = muon_mass.at(dau1_index);
                        dau2_pt = tau_pt.at(dau2_index);
                        dau2_mass = tau_mass.at(dau2_index);
                    } else if (pairType == 1) {
                        dau1_pt = electron_pt.at(dau1_index);
                        dau1_mass = electron_mass.at(dau1_index);
                        dau2_pt = tau_pt.at(dau2_index);
                        dau2_mass = tau_mass.at(dau2_index);
                    } else if (pairType == 2) {
                        dau1_pt = tau_pt.at(dau1_index);
                        dau1_mass = tau_mass.at(dau1_index);
                        dau2_pt = tau_pt.at(dau2_index);
                        dau2_mass = tau_mass.at(dau2_index);
                    } else if (pairType == 3) {
                        dau1_pt = muon_pt.at(dau1_index);
                        dau1_mass = muon_mass.at(dau1_index);
                        dau2_pt = muon_pt.at(dau2_index);
                        dau2_mass = muon_mass.at(dau2_index);
                    } else if (pairType == 4) {
                        dau1_pt = electron_pt.at(dau1_index);
                        dau1_mass = electron_mass.at(dau1_index);
                        dau2_pt = electron_pt.at(dau2_index);
                        dau2_mass = electron_pt.at(dau2_index);
                    } else if (pairType == 5) {
                        dau1_pt = muon_pt.at(dau1_index);
                        dau1_mass = muon_mass.at(dau1_index);
                        dau2_pt = electron_pt.at(dau2_index);
                        dau2_mass = electron_pt.at(dau2_index);
                    } else {
                        return {-999., -999., -999., -999.};
                    }
                    return {dau1_pt, dau1_mass, dau2_pt, dau2_mass};
                }
            """)

    def run(self, df):
        branches = "dau1_pt{0}, dau1_mass{0}, dau2_pt{0}, dau2_mass{0}".format(self.lep_syst)
        branches = branches.split(", ")

        all_branches = df.GetColumnNames()
        if branches[0] in all_branches:
            return df, []

        df = df.Define("lepton_values%s" % self.lep_syst, "get_lepton_values("
            "pairType, dau1_index, dau2_index, "
            "Muon_pt{0}, Muon_mass{0}, Electron_pt{1}, Electron_mass{1}, "
            "Tau_pt{2}, Tau_mass{2})".format(self.muon_syst, self.electron_syst, self.tau_syst))

        for ib, branch in enumerate(branches):
            df = df.Define(branch, "lepton_values%s[%s]" % (self.lep_syst, ib))

        return df, branches


def HHLeptonVarRDF(**kwargs):
    """
    Returns the pt and mass of the selected taus possibly including systematics

    Lepton systematics (used for pt and mass variables) can be modified using the parameters from 
    :ref:`BaseModules_JetLepMetSyst`.

    Required RDFModules: :ref:`HHLepton_HHLeptonRDF`.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHLeptonVarRDF
            path: Tools.Tools.HHLepton
            parameters:
                isMC: self.dataset.process.isMC

    """
    return lambda: HHLeptonVarRDFProducer(**kwargs)


class HHDiTauJetRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHDiTauJetRDFProducer, self).__init__(*args, **kwargs)
        if not os.getenv("HH_DITAUJET"):
            os.environ["HH_DITAUJET"] = "true"
            
            ROOT.gInterpreter.Declare("""
                using Vfloat = const ROOT::RVec<float>&;
                using VInt = const ROOT::RVec<int>&;
                float deltaR(float eta_1, float eta_2, float phi_1, float phi_2) {
                   const float deta = eta_1 - eta_2;
                   const float dphi = ROOT::Math::VectorUtil::Phi_mpi_pi(phi_1 - phi_2);
                   const float dRsq = std::pow(deta,2) + std::pow(dphi,2);
                   return sqrt(dRsq);
                }
                int pass_ditaujet(
                    int pairType, int isTauTauJetTrigger,
                    Vfloat tau_eta, Vfloat tau_phi, int dau1_index, int dau2_index,
                    Vfloat jet_pt, Vfloat jet_eta, Vfloat jet_phi)
                {
                    if (pairType != 2 || isTauTauJetTrigger != 1)  // tautau channel, ditau+jet trig
                        return 1;
                    float dau1_eta = tau_eta[dau1_index];
                    float dau2_eta = tau_eta[dau2_index];
                    float dau1_phi = tau_phi[dau1_index];
                    float dau2_phi = tau_phi[dau2_index];
                    for (int ijet = 0; ijet < jet_pt.size(); ijet++) {
                        if (jet_pt[ijet] < 65)  // 60 from the HLT Path + 5
                            continue;
                        // Overlap removal criteria
                        if ((deltaR(dau1_eta, jet_eta[ijet], dau1_phi, jet_phi[ijet]) > 0.5) &&
                                (deltaR(dau2_eta, jet_eta[ijet], dau2_phi, jet_phi[ijet]) > 0.5))
                            return 1;
                    }
                    return 0;
                }
            """)

    def run(self, df):
        df = df.Define("passTauTauJet",
            "pass_ditaujet(pairType, isTauTauJetTrigger, "
                "Tau_eta, Tau_phi, dau1_index, dau2_index, "
                "Jet_pt{0}, Jet_eta, Jet_phi)".format(self.jet_syst))
        return df.Filter("passTauTauJet == 1"), []


def HHDiTauJetRDF(**kwargs):
    """
    Filters events passing the ditau+jet trigger but not the offline criteria

    Lepton systematics (used for pt and mass variables) can be modified using the parameters from 
    :ref:`BaseModules_JetLepMetSyst`.

    Required RDFModules: :ref:`HHLepton_HHLeptonRDF`.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: HHDiTauJetRDF
            path: Tools.Tools.HHLepton
            parameters:
                isMC: self.dataset.process.isMC

    """
    return lambda: HHDiTauJetRDFProducer(**kwargs)
